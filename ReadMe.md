一、文档说明：
分块文档：
	1412684block + 分块块数 + _ + 文件块编号0开始 + .txt------选择分块算法，存储到磁盘时的文件名
		内容格式：node degree dest1 dest2 dest3...

结果文档：前100个链接以及PageRank的值
	1412684result1_+ 用户输入的alpha值 +.txt------basic PageRank algorithm的运行结果
	1412684result2_+ 用户输入的alpha值 +.txt------optimized PageRank algorithm的运行结果
	1412684result3_+ 用户输入的alpha值 +.txt------Block-Stripe Update algorithm的运行结果
		内容格式：node score		
		
二、运行要求：
1.要求将WikiData.txt和可执行jar文件在同一文件夹下运行
2.其他运行要求按照提示执行即可

三、源代码：
package prework：
	ReadFile------读取数据文件WikiData.txt
		getStatistics(String fileName)------第一次扫描获取基本统计信息
		ReadMyFile(String fileName)------第二次扫描将文件链接关系读入hashMap,放在内存里
		ReadMyFile(String fileName,int block)------第二次扫描将文件链接关系分块写入磁盘文件中

package calculate：
	BasicPR------basic PageRank algorithm	处理蜘蛛网，不处理死节点
	OptimizedPR------optimized PageRank algorithm	处理蜘蛛网和死节点
	BlockStripePR------Block-Stripe Update algorithm	将文件分块处理
	
package demo：
	MyRank------运行程序入口main方法