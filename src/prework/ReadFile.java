package prework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ReadFile {
	private int nodes;//节点数(页面个数)
	private int edges;//边数(链接指向个数)
	private int deadEnds;//死节点个数dead ends
	private int blockNum;//分块算法的块数
	private Map<Integer,Integer> orderMap;//key:node value:order,node大小顺序排列的序号
	private List<Integer> orderList;//按照node大小顺序排列的list
	private Map<Integer,List<Integer>> memMap;//key:node value:key节点的出链的list
	private BufferedWriter[] outWriter;//分块写入文件的writer数组
	private String outFilename[];//分块写入文件的文件名数组
	
 	public ReadFile(){
		this.blockNum = 1;
		this.nodes = 0;
		this.edges = 0;
		this.deadEnds = 0;
		orderMap = new HashMap<Integer,Integer>();
	}
	
	public void setBlockNum(int block){
		this.blockNum = block;
		outWriter = new BufferedWriter[blockNum];
		outFilename = new String[blockNum];
		for(int i = 0;i < blockNum;i++)
		{
			outFilename[i] = "1412684block"+block+"_"+i+".txt";
			File file = new File(outFilename[i]);
			// 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
	        if (file.exists() && file.isFile()) {
	            if (file.delete()) {
	                System.out.println("删除单个文件" + outFilename[i] + "成功！");
	            } 
	        }
			try {
				outWriter[i] = new BufferedWriter(new FileWriter(outFilename[i], true));
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
	}
	
	public Map<Integer, Integer> getOrderMap() {
		return orderMap;
	}

	public List<Integer> getOrderList() {
		return orderList;
	}

	public int getNodes() {
		return nodes;
	}

	public Map<Integer, List<Integer>> getMemMap() {
		return memMap;
	}

	public void setMemMap(Map<Integer, List<Integer>> memMap) {
		this.memMap = memMap;
	}

	public String[] getOutFilename() {
		return outFilename;
	}

	//第一次扫描获取基本统计信息
	public void getStatistics(String fileName)
	{
		BufferedReader reader = null;
		String tempString=null;
		Set<Integer> nodeSet = new HashSet<Integer>();
		Set<Integer> nodeLive = new HashSet<Integer>();
		int edge = 0;
		
		try {
			reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(fileName)),5*1024*1024);
					
			int fromNode = -1, toNode = -1;
			
			while((tempString=reader.readLine())!=null)
			{
				String[] tempSpilt = tempString.split("\\s+");
				
				fromNode = Integer.parseInt(tempSpilt[0]);
				toNode = Integer.parseInt(tempSpilt[1]);
				nodeSet.add(fromNode);
				nodeSet.add(toNode);
				nodeLive.add(fromNode);
				
				edge++;
			}
			reader.close();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.nodes = nodeSet.size();
		this.edges = edge;
		this.deadEnds = nodeSet.size() - nodeLive.size();
		sortNodes(nodeSet);
		
		System.out.println("节点数	"+this.nodes);
		System.out.println("边数	"+this.edges);
		System.out.println("死节点数	"+this.deadEnds);
	}
	
	//对节点排序,将node与id对应好
	public void sortNodes(Set<Integer> nodeSet){
		orderList = new ArrayList<Integer>(nodeSet);
		Collections.sort(orderList);
		//遍历列表,将排序信息写入Map
		for(int i = 0;i < orderList.size();i++){
			this.orderMap.put(orderList.get(i), i);
		}
	}
	
	//将文件链接关系读入hashMap,放在内存里
	public void ReadMyFile(String fileName){
		BufferedReader reader = null;
		String tempString=null;
		//Map<node,outLinkList>
		Map<Integer,List<Integer>> memMap = new HashMap<Integer,List<Integer>>();
		
		try{
			//读大文件，设置缓存
			reader = new BufferedReader(new InputStreamReader(
			new FileInputStream(fileName)),5*1024*1024);
			
			int fromNode = -1, toNode = -1;
			List<Integer> outLinks = new ArrayList<Integer>();//存放某个节点出链的列表
			
			while((tempString=reader.readLine())!=null)
			{
				String[] tempSpilt = tempString.split("\\s+");
				
				//每次读到一个新节点,将上一个节点的相关信息add到memMap中
				if(fromNode != Integer.parseInt(tempSpilt[0]) && fromNode != -1){
					memMap.put(fromNode, outLinks);
					//重新构造一个list
					outLinks = new ArrayList<Integer>();
				}
				
				//数据解析
				fromNode = Integer.parseInt(tempSpilt[0]);
				toNode = Integer.parseInt(tempSpilt[1]);
				//将新的出链add到list中
				outLinks.add(toNode);
			}
			//处理最后一个节点
			memMap.put(fromNode, outLinks);
			setMemMap(memMap);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e){
		    e.printStackTrace();
		}finally{
		    if(reader!=null)
		    {
		    	try{
		            reader.close();
		        }catch(IOException e)
		        {
		            e.printStackTrace();
		        }
		    }
		}
		setMemMap(memMap);
	}
	
	//读文件,(并分块)写入文件
	public void ReadMyFile(String fileName,int block){
		BufferedReader reader = null;
		String tempString=null;
		setBlockNum(block);
		
		try{
			//读大文件，设置缓存
			reader = new BufferedReader(new InputStreamReader(
			new FileInputStream(fileName)),5*1024*1024);
			
			int fromNode = -1, toNode = -1;
			List<Integer> outLinks = new ArrayList<Integer>();
			
			while((tempString=reader.readLine())!=null)
			{
				String[] tempSpilt = tempString.split("\\s+");
				
				if(fromNode != Integer.parseInt(tempSpilt[0]) && fromNode != -1){
					//写入文件
					writeBlocks(fromNode,outLinks);
					outLinks.clear();
				}
				
				fromNode = Integer.parseInt(tempSpilt[0]);
				toNode = Integer.parseInt(tempSpilt[1]);
				outLinks.add(toNode);
			}
			
			//写入文件
			writeBlocks(fromNode,outLinks);
			
			//关闭输出流
			for(int i = 0;i < blockNum;i++)
			{
				if (outWriter[i] != null) { 
	    	         try {
	    	        	 outWriter[i].close();
					} catch (IOException e) {
						e.printStackTrace();
					} 
	    	    } 
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e){
		    e.printStackTrace();
		}finally{
		    if(reader!=null)
		    {
		    	try{
		            reader.close();
		        }catch(IOException e)
		        {
		            e.printStackTrace();
		        }
		    }
		}
	}

	//将src,degree,destinations逐行写入文件
	void writeBlocks(int fromNode,List<Integer> outLinks){
		
		int degree = outLinks.size();
		List<Integer> outList = new ArrayList<Integer>();
		//计算块大小
		int blockSize = this.nodes / this.blockNum + 1;
//		System.out.println(blockSize);
		//计算块索引
		int index = -1,orderId;
		
		for(int toNode:outLinks)
		{
			//目标节点的排序序号
			orderId = this.orderMap.get(toNode);
			if(index != (orderId / blockSize) && index != -1){
//				System.out.println(toNode+" "+orderId + " " + index);
				writeLine(fromNode,degree,index,outList);
			}
			index = orderId / blockSize;
			outList.add(toNode);
//			System.out.println(toNode+" "+orderId + " " + index);
		}
		writeLine(fromNode,degree,index,outList);
	}
	
	private void writeLine(int fromNode,int degree,int index,List<Integer> outList){
		try {
			outWriter[index].write(fromNode + " " + degree);
			for(int dests:outList){
				outWriter[index].write(" " + dests);
			}
			outWriter[index].write("\n");
			outList.clear();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
