package demo;

import java.util.Scanner;

import calculate.BasicPR;
import calculate.BlockStripePR;
import calculate.OptimizedPR;
import prework.ReadFile;

public class MyRank {

	public static void main(String[] args) {
		
		String fileName = "WikiData.txt";
		ReadFile readFile = new ReadFile();
		System.out.println("文件WikiData.txt的统计信息");
		readFile.getStatistics(fileName);
		Scanner s1 = new Scanner(System.in);
		boolean isRuning = true;
		while(isRuning){
			System.out.println("\n0.basic PageRank algorithm	处理蜘蛛网,不处理死节点");
			System.out.println("1.optimized PageRank algorithm	处理蜘蛛网和死节点");
			System.out.println("2.Block-Stripe Update algorithm	将文件分块处理");
			System.out.println("3.退出程序");
			System.out.print("请选择PageRank的算法的序号：");
			int choice = s1.nextInt();
			if(choice == 3){
				isRuning = false;
				s1.close();
				break;
			}
			System.out.print("请输入PageRank的算法的beta值(在0.8~0.9之间选择),推荐选择0.85,如果选择1则不处理蜘蛛网：");
			float alpha = s1.nextFloat();
			switch(choice){
				case 0:
					//basic PageRank algorithm
					readFile.ReadMyFile(fileName);
					BasicPR basicPR = new BasicPR(readFile.getNodes(),readFile.getOrderMap(),
							readFile.getOrderList(),readFile.getMemMap());
					basicPR.pageRank(alpha);
					break;
				case 1:
					//optimized PageRank algorithm(handling dead ends)
					readFile.ReadMyFile(fileName);
					OptimizedPR optimizedPR = new OptimizedPR(readFile.getNodes(),readFile.getOrderMap(),
							readFile.getOrderList(),readFile.getMemMap());
					optimizedPR.pageRank(alpha);
					break;
				case 2:
					//Block-Stripe Update algorithm
					//节点数目size=7115,推荐每块分size * 500,共分为size/500+1块
					System.out.print("请选择PageRank的分块算法的块数,推荐15：");
					int blockNum = s1.nextInt();
					System.out.print("是否处理死节点(0.不处理	1.处理)：");
					int isDeal = s1.nextInt();
					readFile.ReadMyFile(fileName,blockNum);
					BlockStripePR blockStripePR = new BlockStripePR(readFile.getNodes(),
							readFile.getOrderMap(),readFile.getOrderList(),
							readFile.getOutFilename(),isDeal);
					blockStripePR.pageRank(alpha);
					break;
				case 3:
					isRuning = false;
					s1.close();
					break;
				default:
					break;
			}
		}
	}
	
}
