package calculate;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/*
 * handle dead ends and spider traps
 */
public class OptimizedPR {

	private int size;
	private float alpha;
	private Float[] oldRank;
	private Float[] newRank;
	private Map<Integer,Integer> orderMap;//node,id
	private List<Integer> orderList;
	private Map<Integer,List<Integer>> memMap;
	
	public OptimizedPR(int size,Map<Integer,Integer> orderMap, List<Integer> orderList,
			Map<Integer,List<Integer>> memMap){
		this.size = size;
		this.oldRank = new Float[size];
		this.newRank = new Float[size];
		this.orderMap = orderMap;
		this.orderList = orderList;
		this.memMap = memMap;
		for(int i = 0;i < size;i++){
			oldRank[i] = (float)(1/(float)size);
		}
	}
	
	public void pageRank(float alpha){
		this.alpha = alpha;
		File file = new File("1412684result2_"+alpha+".txt");
		// 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                System.out.println("删除单个文件" + "1412684result2_"+alpha+".txt" + "成功！");
            } 
        }
		float limit = 1.0e-6f;
		//迭代计算，直到数据收敛
        while(true) {
        	//一次迭代矩阵相乘,得到newRank
        	multiply();
        	// 计算是否收敛    
        	float count = 0;
        	for(int j = 0;j < size;j++){
        		count += Math.abs(oldRank[j] - newRank[j]);
    		}
            // 拷贝最新的数组值到数组oldRank
        	for(int j = 0;j < size;j++){
    			oldRank[j] = newRank[j];
    		}
            
            if (count <= limit) {
                break;
            } 
        }
        sort();
	}
	
	private void multiply(){
		for(int i = 0;i < size;i++){
			newRank[i] = 0.0f;
		}
		
//		for(int i = 0;i < size;i++){
//			for(int j = 0;j < size;j++){
//				//matrix[i][j]的值
//				int toNode = orderList.get(i);
//				int fromNode = orderList.get(j);
//				if(memMap.containsKey(fromNode)){//不是死节点
//					if(memMap.get(fromNode).contains(toNode)){
//						int degree = memMap.get(fromNode).size();
//						newRank[i] += alpha * oldRank[j] * (float)(1/(float)degree);
//					}
//				}
//			}
//			rankSum += newRank[i];
//		}
		//遍历memMap
		Iterator<Entry<Integer, List<Integer>>> it = memMap.entrySet().iterator();
		while(it.hasNext()){
			Entry<Integer, List<Integer>> entry = (Entry<Integer, List<Integer>>)it.next();
			int fromNode = (int)entry.getKey();
			int i = orderMap.get(fromNode);
			List<Integer> outList = (List<Integer>)entry.getValue();
			int degree = outList.size();
			//遍历destNodes
			for(int toNode:outList){
				int j = orderMap.get(toNode);
				newRank[j] += alpha * oldRank[i] * (float)(1/(float)degree);
			}
		}
		float rankSum = 0.0f;//累加变量
		for(int i = 0;i < size;i++){
			rankSum += newRank[i];//累加全部rank值,公式中的S
		}
		//handle dead ends
		for(int i = 0;i < size;i++){
			//分配(1-S)给N个节点,处理值泄露
			newRank[i] += (1 - rankSum)/(float)size;
		}
	}
	
	private void sort(){
		//排名
		int r[] = new int[size];
		for(int i = 0;i < size;i++)
			r[i] = 0;
		for(int i = 1;i < size;i++){
			for(int j = 0;j < i;j++)
				if(oldRank[j] <= oldRank[i])
					r[i]++;
				else
					r[j]++;
		}
		
		HashMap<Integer,Integer> map = new HashMap<Integer,Integer>();
		float sum = 0.0f;
        for (int i = 0; i < size; i++) {
        	if(oldRank[i]>=0)
        		map.put(r[i],i); // 将值和下标存入Map
        	sum += oldRank[i];
        }
        System.out.println(sum);
        for (int i = size; i > size - 100; i--) {
        	int id = map.get(i-1);
        	System.out.println(orderList.get(id) + " " + oldRank[id]);
        	try {
				BufferedWriter writer = new BufferedWriter(
						new FileWriter("1412684result2_"+alpha+".txt", true));
				writer.write(orderList.get(id) + " " + oldRank[id] +"\n");
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			} 
        }
	}
}
