package calculate;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BlockStripePR {
	private int size;
	private float alpha;
	private Float[] oldRank;
	private Float[] newRank;
	private Map<Integer,Integer> orderMap;//node,id
	private List<Integer> orderList;
	private String outFilename[];
	private boolean flag;

	public BlockStripePR(int size,Map<Integer,Integer> orderMap, List<Integer> orderList,
			String outFilename[],int isDeal){
		this.size = size;
		this.oldRank = new Float[size];
		this.newRank = new Float[size];
		this.orderMap = orderMap;
		this.orderList = orderList;
		this.outFilename = outFilename;
		if(isDeal > 0){
			this.flag = true;
		}else{
			this.flag = false;
		}
		for(int i = 0;i < size;i++){
			oldRank[i] = (float)(1/(float)size);
		}
	}
	
	public void pageRank(float alpha){
		this.alpha = alpha;
		File file = new File("1412684result3_"+alpha+".txt");
		// 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                System.out.println("删除单个文件" + "1412684result3_"+alpha+".txt" + "成功！");
            } 
        }
        float limit = 1.0e-6f;
		//迭代计算，直到数据收敛
        while(true) {
        	//一次迭代矩阵相乘,得到newRank
        	multiply();
        	// 计算是否收敛    
        	float count = 0;
        	for(int j = 0;j < size;j++){
        		count += Math.abs(oldRank[j] - newRank[j]);
    		}
            // 拷贝最新的数组值到数组oldRank
        	for(int j = 0;j < size;j++){
    			oldRank[j] = newRank[j];
    		}
            
            if (count <= limit) {
                break;
            } 
        }
        sort();
	}
	
	private void multiply(){
		for(int i = 0;i < size;i++){
			newRank[i] = 0.0f;
		}
		//循环读取文件,计算newRank
		for(int i = 0;i < outFilename.length;i++){
			BufferedReader reader = null;
			String tempString=null;
			try {
				reader = new BufferedReader(new InputStreamReader(
						new FileInputStream(outFilename[i])),5*1024*1024);
				while((tempString=reader.readLine())!=null)
				{
					String[] tempSpilt = tempString.split("\\s+");
					int fromNode = -1, degree = 0,toNode = -1;
					fromNode = Integer.parseInt(tempSpilt[0]);
					int fromId = orderMap.get(fromNode);
					degree = Integer.parseInt(tempSpilt[1]);
					for(int j = 2;j < tempSpilt.length;j++){
						toNode = Integer.parseInt(tempSpilt[j]);
						int toId = orderMap.get(toNode);
						newRank[toId] += alpha * oldRank[fromId] * (float)(1/(float)degree);
					}
				}
				reader.close();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(flag){
			float rankSum = 0.0f;
			for(int i = 0;i < size;i++){
				rankSum += newRank[i];
			}
			//handle dead ends
			for(int i = 0;i < size;i++){
				newRank[i] += (1 - rankSum)/(float)size;
			}
		}else{
			for(int i = 0;i < size;i++){
				newRank[i] += (1 - alpha)/(float)size;
			}
		}
	}
	
	private void sort(){
		//排名
		int r[] = new int[size];
		for(int i = 0;i < size;i++)
			r[i] = 0;
		for(int i = 1;i < size;i++){
			for(int j = 0;j < i;j++)
				if(oldRank[j] <= oldRank[i])
					r[i]++;
				else
					r[j]++;
		}
		
		HashMap<Integer,Integer> map = new HashMap<Integer,Integer>();
		float sum = 0.0f;
        for (int i = 0; i < size; i++) {
        	if(oldRank[i]>=0)
        		map.put(r[i],i); // 将值和下标存入Map
        	sum += oldRank[i];
        }
        System.out.println(sum);
        for (int i = size; i > size - 100; i--) {
        	int id = map.get(i-1);
        	System.out.println(orderList.get(id) + " " + oldRank[id]);
        	try {
				BufferedWriter writer = new BufferedWriter(
						new FileWriter("1412684result3_"+alpha+".txt", true));
				writer.write(orderList.get(id) + " " + oldRank[id] +"\n");
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			} 
        }
	}
}
